<?php
namespace OCA\Gestion\Controller;


use OC\DatabaseException;
use OC\DB\Connection;
use OC\DB\ConnectionFactory;
use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use \OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\StrictContentSecurityPolicy;
use OCP\ILogger;
use OCP\Contacts\IManager;

use Exception;



class ContactsController extends Controller {
    private $userId;
    private $contactsManager;


    public function __construct(IManager $contactsManager, ILogger $logger, $AppName, IRequest $request, $UserId){
        parent::__construct($AppName, $request);
        $this->logger = $logger;
        $this->userId = $UserId;
        $this->contactsManager = $contactsManager;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     */
    public function index() {



        $params = [

            // Contacts est basé sur le fichier vcf FN:val Email:val
            "contacts" => $this->contactsManager->search($filter ?: '', [
                'FN',
                'EMAIL',

            ]),

        ];

        $response = new TemplateResponse('gestion', 'index', $params);

        $csp = new StrictContentSecurityPolicy();
        $csp->allowEvalScript();
        $csp->allowInlineStyle();

        $response->setContentSecurityPolicy($csp);

        return $response;
    }

    /**
     * @NoAdminRequired
     *
     * @param int $id
     */
    public function show($id){

        $params = [

            // Contacts est basé sur le fichier vcf FN:val Email:val
            "contacts" => $this->contactsManager->search($filter ?: '', [
                'FN',
                'EMAIL',
                'TEL',
                'ADR',
            ]),

        ];

        $response = new TemplateResponse('gestion', 'test', $params);

        $csp = new StrictContentSecurityPolicy();
        $csp->allowEvalScript();
        $csp->allowInlineStyle();

        $response->setContentSecurityPolicy($csp);

        return $response;
    }




}