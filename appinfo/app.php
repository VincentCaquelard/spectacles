<?php

namespace OCA\Gestion\AppInfo;

\OC::$server->getNavigationManager()->add([
	'id' => 'gestion',
    'order' => 9,
	'href' => \OC::$server->getURLGenerator()->linkToRoute('gestion.contacts.index'),
	'name' => \OCP\Util::getL10N('gestion')->t('Gestion')
]);
