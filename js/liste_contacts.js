(function(){
    if(!OCA.Gestion) {
        /**
         * Namespace for the files app
         * @namespace OCA.Gestion
         */
        OCA.Gestion = {};
    }

    /**
     * @namespace OCA.Gestion.Liste_contacts
     */
    OCA.Gestion.Liste_contacts = {
        initialize: function() {
            $('#contact').on('click',_.bind(this._clickContacts, this));

        },

        _clickContacts: function() {

            var baseUrl = OC.generateUrl('/apps/gestion');

            var id = 1;
            $.ajax({
                url: baseUrl + '/contact/' + id,
                type: 'POST'
            }).done(function (response) {
                alert('succes');
            }).fail(function (response, code) {
                // handle failure
            });
        }

        // example for calling the PUT /notes/1 URL

    }
})();

$(document).ready(function(){
    OCA.Gestion.Liste_contacts.initialize();
});
