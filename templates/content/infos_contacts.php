<h2>Gestion du contact</h2>

<fieldset>
    <legend>Coordonnées</legend> <!-- Titre du fieldset -->

    <label for="nom">Nom</label>
    <input type="text" name="nom" id="nom" value="<?php p($c['FN']); ?>"/>

    <label for="email">Email 1</label>
    <input type="email" name="email" id="email" value="<?php p($c['EMAIL'][0]); ?>" />

    <label for="tel1">Telephone bureau</label>
    <input type="tel1" name="tel1" id="tel1" value="<?php p($c['TEL'][0]); ?>" />

    <label for="tel2">Telephone portable</label>
    <input type="tel2" name="tel2" id="tel2" value="<?php p($c['TEL'][1]); ?>" />

    <label for="email">Email 1</label>
    <input type="email" name="email" id="email" value="<?php p($c['EMAIL'][0]); ?>" />

    <label for="email2">Email 2</label>
    <input type="email2" name="email2" id="email2" value="<?php p($c['EMAIL'][1]); ?>"/>

    <label for="addr">Adresse</label>
    <input type="addr" name="addr" id="addr" value="<?php p($c['ADR']); ?>"/>


</fieldset>

<fieldset>
    <legend>Représentations</legend> <!-- Titre du fieldset -->

    <p>
        <strong>Contexte de représentation :<strong></strong>
            <br/>
            Lieu:

            <input type="radio" name="lieu" value="Salle" id="salle" /> <label for="salle">Salle"</label>
            <input type="radio" name="lieu" value="Exterieur" id="ext" /> <label for="ext">Exterieur</label>
            <input type="radio" name="lieu" value="Rue" id="rue" /> <label for="rue">Rue</label>

            Public:
            <input type="radio" name="public" value="jeunePublic" id="jeunePublic" /> <label for="jeunePublic">Jeune Public"</label>
            <input type="radio" name="publioc" value="tousPublic" id="tousPublic" /> <label for="tousPublic">Tous Public</label>

    </p>


</fieldset>

<input type="submit" id="submitContacts" value="<?php p($l->t('Enregistrer')); ?>"/>

