<?php
\OCP\Util::addScript('gestion', 'liste_contacts');
?>

<div>
    <h2 class="titre">Contacts </h2>

    <div class="liste_contacts">

            <?php foreach($_['contacts'] as $c) { ?>
                <div class ="contact" id="contact">
                    <a href="/nextcloud/index.php/apps/gestion/contact/<?php p($c['UID']); ?>">X</a>
                    <p  class="logo_contact"><?php p($c['FN'][0]); ?></p>
                    <div class="contact_name">
                         <p><?php p($c['FN']); ?></p>
                         <p class="email"><?php p($c['EMAIL'][0]); ?></p>
                    </div>
                </div>
            <?php } ?>

    </div>

</div>